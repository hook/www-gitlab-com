---
layout: markdown_page
title: "Build"
---

## Cloud Images
The process documenting the steps necessary to update the GitLab images available on
the various cloud providers is detailed on our [Cloud Image Process page](https://about.gitlab.com/cloud-images/).
